<?php 
    include("db.php");

    // aca lo que haces es trerte la info...
    
    
    if (isset($_GET['id'])) {

        $id = $_GET['id'];
        $query = "SELECT * FROM tareas WHERE Id = $id";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) == 1) {
            $row = mysqli_fetch_array($result);
            $title = $row['Titulo'];
            $description = $row['Descripcion'];
            
        }   
        if(isset($_POST['update'])){  
            echo 'updating';
        }
    
    }
    
  

?>

<?php include("Includes/header.php") ?>

<div class="container p-4">
    <div class="row">
        <div class="col -md-4 mx-auto">
            <div class="card card-body">
                <form action="edit_task.php?id=<?php echo $_GET['id']; ?>" method="POST"> <!-- aca le indicas a donde vas a mandar el formulario esta ok -->
                    <div class="form-group">
                        <input type="text" name='Titulo' value="<?php echo $title; ?>" 
                        class="form-control" placeholder="Actualiza el titulo"> 
                    </div>
                    <div class="form-group">
                        <textarea name="Descripcion"  rows="2" class="form-control" 
                        placeholder= "Actualiza la descripción"><?php echo $description; ?></textarea> 
                    </div>
                    <button class="btn btn-success" name="update"> <!-- con este boton le envias toda la informacion al formulario que queres editar -->
                        Update
                    </button>
                </form>
            </div>        
        </div>
    </div>
</div>

<?php include("Includes/fooder.php") ?>